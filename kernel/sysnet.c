//
// network system calls.
//

#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "riscv.h"
#include "spinlock.h"
#include "proc.h"
#include "defs.h"
#include "fs.h"
#include "sleeplock.h"
#include "file.h"
#include "net.h"

struct sock {
  struct sock *next; // the next socket in the list
  uint32 raddr;      // the remote IPv4 address
  uint16 lport;      // the local UDP port number
  uint16 rport;      // the remote UDP port number
  struct spinlock lock; // protects the rxq
  struct mbufq rxq;  // a queue of packets waiting to be received
};

static struct spinlock lock;
static struct sock *sockets;

void
sockinit(void)
{
  initlock(&lock, "socktbl");
}

int
sockalloc(struct file **f, uint32 raddr, uint16 lport, uint16 rport)
{
  struct sock *si, *pos;

  si = 0;
  *f = 0;
  if ((*f = filealloc()) == 0)
    goto bad;
  if ((si = (struct sock*)kalloc()) == 0)
    goto bad;

  // initialize objects
  si->raddr = raddr;
  si->lport = lport;
  si->rport = rport;
  initlock(&si->lock, "sock");
  mbufq_init(&si->rxq);
  (*f)->type = FD_SOCK;
  (*f)->readable = 1;
  (*f)->writable = 1;
  (*f)->sock = si;

  // add to list of sockets
  acquire(&lock);
  pos = sockets;
  while (pos) {
    if (pos->raddr == raddr &&
        pos->lport == lport &&
        pos->rport == rport) {
      release(&lock);
      goto bad;
    }
    pos = pos->next;
  }
  si->next = sockets;
  sockets = si;
  release(&lock);
  return 0;

bad:
  if (si)
    kfree((char*)si);
  if (*f)
    fileclose(*f);
  return -1;
}

//
// Your code here.
//
// Add and wire in methods to handle closing, reading,
// and writing for network sockets.
//

void sockclose(struct sock* s){
  //printf("sockclose starts\n");
  // For the close method, remove the socket from the sockets list. 
  struct sock *pos, *prev; 
  pos = sockets;
  prev = 0;
  while(pos){
    //printf("s: %p, pos: %p, pos->next: %p\n", s, pos, pos->next);
    if(s == pos){
      if(prev)
        prev->next = pos->next;
      else
        sockets = pos->next;
      break;
    }
    prev = pos;
    pos = pos->next;
  }

  // Then, free the socket object. Be careful to free any mbufs that have not been read first, before freeing the struct sock.
  // TODO: Why do I need to care freeing mbufs that have not been read yet? --> Some processes might be sleeping and waiting for mbuf.
  if(!pos){
    if(!mbufq_empty(&pos->rxq)){
      while(1){
        if(mbufq_empty(&pos->rxq))
          break;
        wakeup(pos);
      }

//      struct mbuf *sm = pos->rxq.head, *nm=0;
//      while(1){
//        nm = sm->next;   
//        mbuffree(sm);
//       
//        sm = nm;
//        if(!sm)
//          break;
//      }
    }
    kfree(pos);
  }
}

// s: pointer to socket
// addr: user virtual address
// n: length of data that will be sent
int sockread(struct sock* s, uint64 addr, int n){
  int ret_len;
  // For the read method, check if rxq is empty using mbufq_empty(), and if it is, use sleep() to wait until an mbuf is enqueued. 
  // (Don't forget to wrap the sleep() call into a while loop as other usages of sleep() do). 
  acquire(&s->lock);
  while(1){ 
    if(!mbufq_empty(&s->rxq))
      break;
    sleep(s, &s->lock); // TODO: what is *chan?
  }

  // Using mbufq_pophead, pop the mbuf from rxq and use copyout() to move its payload into user memory. 
  struct mbuf *mbe = mbufq_pophead(&s->rxq);
  release(&s->lock); // Not need to use socket s anymore

  pagetable_t pgt = mycpu()->proc->pagetable;
  if(mbe->len >= n){
    copyout(pgt, addr, mbe->head, n);
    ret_len = n;
  }else if(mbe->len < n){
    copyout(pgt, addr, mbe->head, mbe->len);
    ret_len = mbe->len;
  }

  // Free the mbuf using mbuffree() to finish.
  if(mbe->len <= n)
    mbuffree(mbe);

  return ret_len;
}


// s: pointer to socket
// addr: user virtual address
// n: length of data that will be sent
// TODO: What if n is larger than the maximum length of buf in mbuf? --> We don't need to care about this in this lab.
int sockwrite(struct sock* s, uint64 addr, int n){
  // For the write method, allocate a new mbuf, taking care to leave enough headroom for the UDP, IP, and Ethernet headers. 
  // TODO: What's the meaning of 'enough headroom'? --> length of UDP, IP, and Ethernet headers
  struct mbuf *new_mbuf = mbufalloc(sizeof(struct eth) + sizeof(struct ip) + sizeof(struct udp)); 

  // Use mbufput() and copyin() to transfer the payload from user memory into the mbuf. 
  pagetable_t pgt = mycpu()->proc->pagetable;
  copyin(pgt, new_mbuf->head, addr, n);
  mbufput(new_mbuf, n); // TODO: how much do I have to put? --> length of the payload

  // Finally, use net_tx_udp() to send the mbuf.
  net_tx_udp(new_mbuf, s->raddr, s->lport, s->rport);

  return n;
}


// called by protocol handler layer to deliver UDP packets
void
sockrecvudp(struct mbuf *m, uint32 raddr, uint16 lport, uint16 rport)
{
  // Find the socket that handles this mbuf and deliver it, waking
  // any sleeping reader. Free the mbuf if there are no sockets
  // registered to handle it.
  //

  // For sockrecvudp(), identify which socket (if any) can handle the mbuf and deliver it to the appropriate rxq. 
  struct sock *pos = sockets;
  while(pos){
    if(pos->raddr == raddr &&
      pos->lport == lport &&
      pos->rport == rport){
      acquire(&pos->lock);
      mbufq_pushtail(&pos->rxq, m);
      release(&pos->lock);
      break;
    }

    pos = pos->next; 
  }

  // Use wakeup() to wake any waiting reader.
  if(pos)
    wakeup(pos); //TODO: what is chan?
  else
    mbuffree(m);
}
